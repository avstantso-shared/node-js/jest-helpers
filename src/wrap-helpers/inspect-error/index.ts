import 'jest';
import util from 'util';
import { FailedContext, JestHelper } from '../abstract';

export namespace InspectErrorHelper {
  export type Factory = JestHelper.Factory<InspectErrorHelper> & {
    report(error: any): never;
  };
}

export type InspectErrorHelper = JestHelper;

export const InspectErrorHelper: InspectErrorHelper.Factory = (
  globaleEabled = true
) => {
  let current: JestHelper.Current;
  let error: any;

  const clear = () => {
    error = undefined;
  };

  const report = () =>
    error &&
    `Inspect error:\r\n${util.inspect(error, {
      depth: Infinity,
      maxStringLength: Infinity,
    })}`;

  const make = (): JestHelper.Current => {
    current = { report };
    return current;
  };

  const catcher: JestHelper['catcher'] = (e) => {
    error = e;
  };

  return JestHelper(
    {
      name: InspectErrorHelper.name,
      make,
      clear,
      catcher,
      current: () => current,
      get priority() {
        return 99;
      },
    },
    globaleEabled
  );
};

InspectErrorHelper.report = (error) =>
  FailedContext.report(
    util.inspect(error, { depth: Infinity, maxStringLength: Infinity })
  );
