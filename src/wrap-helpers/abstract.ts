import Bluebird from 'bluebird';

export class FailedContext extends Error {
  constructor(context: any) {
    super(context);
    Object.setPrototypeOf(this, FailedContext.prototype);
  }

  static report<T>(report: T): never {
    const e = new FailedContext(report);
    delete e.stack;
    throw e;
  }
}

export namespace JestHelper {
  export type ItWrapper = {
    enter(): void;
    leave(): void;
    catcher(e: any): void;
  };

  export type Current = {
    report(): string | Promise<string>;
  };

  export type Inner = Partial<Pick<ItWrapper, 'catcher'>> & {
    readonly name: string;
    readonly priority: number;
    make(): JestHelper.Current;
    clear(): void;
    current(): JestHelper.Current;
  };

  export type Factory<T extends JestHelper = JestHelper> = (
    globaleEabled?: boolean
  ) => T;

  export type Union = JestHelper | Factory;
}

export interface JestHelper extends JestHelper.ItWrapper {
  readonly priority: number;
  readonly active: boolean;
  report(): string | Promise<string>;
  throwReport(): void;
  attach(): void;
  it: jest.It;
}

export const makeIt = ({
  enter,
  leave,
  catcher,
}: JestHelper.ItWrapper): jest.It => {
  const itWrap = (
    name: string,
    fn?: jest.ProvidesCallback,
    timeout?: number
  ) => {
    function cth(e: any) {
      if (catcher && !e.matcherResult) catcher(e);

      throw e;
    }

    let extFn: jest.ProvidesCallback;
    if (1 === fn.length)
      extFn = (cb: jest.DoneCallback): void | undefined => {
        enter();

        try {
          fn(cb);
        } catch (e) {
          cth(e);
        }

        leave();
      };
    else
      extFn = (): Promise<unknown> => {
        enter();

        return Bluebird.Promise.resolve()
          .then(() => (fn as Function)())
          .catch(cth)
          .tap(leave);
      };

    return it(name, extFn, timeout);
  };

  return new Proxy(itWrap as jest.It, {
    get(target, p) {
      return it[p as keyof typeof it];
    },
  });
};

export const JestHelper = (
  inner: JestHelper.Inner,
  globaleEabled = true
): JestHelper => {
  const enter = () => {
    if (!globaleEabled) return;

    inner.make();
  };

  const leave = () => {
    if (!globaleEabled) return;

    inner.clear();
  };

  const catcher: JestHelper['catcher'] = (e) => {
    if (!globaleEabled) return;

    inner.catcher && inner.catcher(e);
  };

  const report = (): string | Promise<string> => {
    if (!(globaleEabled && inner.current())) return undefined;

    return inner.current().report();
  };

  const throwReport = async () => {
    if (!(globaleEabled && inner.current())) return;

    const r = await Promise.resolve(inner.current().report());
    inner.clear();

    if (r) FailedContext.report(r);
  };

  const attach = () => {
    if (!globaleEabled) return;

    afterEach(throwReport);
  };

  return {
    get active() {
      return !!inner.current();
    },
    get priority() {
      return inner.priority;
    },
    enter,
    leave,
    catcher,
    report,
    throwReport,
    attach,
    it: globaleEabled ? makeIt({ enter, leave, catcher }) : it,
  };
};
