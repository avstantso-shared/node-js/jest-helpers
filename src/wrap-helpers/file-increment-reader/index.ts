import fs from 'fs';
import fsPromises from 'fs/promises';

import { Generics } from '@avstantso/node-or-browser-js--utils';

export namespace FileIncrementReader {
  export interface Options<T = string> {
    readonly clear?: boolean;
    convert?(aFrom: string): T;
    timeout?: number;
    timeoutMessage?: string;
  }

  export interface Watcher<T = string> {
    read(): Promise<T>;
    cancel(): void;
  }
}

export interface FileIncrementReader<T = string> {
  readonly filename: string;
  watch(): FileIncrementReader.Watcher<T>;
}

export const FileIncrementReader = <T = string>(
  filename: string,
  options?: FileIncrementReader.Options<T>
): FileIncrementReader<T> => {
  const clear = undefined === options?.clear ? false : !!options?.clear;
  const { convert } = options || {};
  const timeout = !options?.timeout ? 10000 : options.timeout;
  const timeoutMessage = !options?.timeoutMessage
    ? `Read for ${filename} time out`
    : options.timeoutMessage;

  if (clear && fs.existsSync(filename)) fs.rmSync(filename);

  const watch = (): FileIncrementReader.Watcher<T> => {
    let readed = (fs.existsSync(filename) && fs.statSync(filename).size) || 0;

    const doReadIncrement = async (stats: fs.Stats): Promise<T> => {
      let result: string = '';

      const filehandle = await fsPromises.open(filename, 'r+');
      try {
        const buffer = Buffer.alloc(2048);

        await filehandle.read(buffer, 0, stats.size - readed, readed);
        result = `${buffer}`.replace(/\0/g, '').trim();
      } finally {
        await filehandle.close();
      }

      return Promise.resolve<T>(
        convert ? convert(result) : Generics.Cast(result)
      );
    };

    let isCanceled = false;
    const cancel = () => (isCanceled = true);

    const read = (): Promise<T> =>
      new Promise<T>((resolve, reject) => {
        const timeoutId = setInterval(() => reject(timeoutMessage), timeout);

        const readIncrementAttempt = () => {
          if (isCanceled) resolve(Generics.Cast(undefined));

          const stats = fs.statSync(filename);

          if (stats.size === readed)
            setTimeout(() => {
              readIncrementAttempt();
            }, 100);
          else {
            clearInterval(timeoutId);

            resolve(doReadIncrement(stats));
          }
        };

        readIncrementAttempt();
      });

    return { read, cancel };
  };

  return { filename, watch };
};
