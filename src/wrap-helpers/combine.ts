import 'jest';
import { JS } from '@avstantso/node-or-browser-js--utils';
import { FailedContext, JestHelper, makeIt } from './abstract';

export namespace CombineHelper {
  export interface Overload {
    (
      globaleEabled: boolean,
      helper: JestHelper.Union,
      ...helpers: JestHelper.Union[]
    ): JestHelper;
    (helper: JestHelper.Union, ...helpers: JestHelper.Union[]): JestHelper;
  }
}

export const CombineHelper: CombineHelper.Overload = (
  ...params: any[]
): JestHelper => {
  const hasGE = JS.is.boolean(params[0]);
  const globaleEabled = (hasGE && params[0]) || true;
  const helpers: JestHelper[] = (() => {
    const hs = [...params];
    if (hasGE) hs.shift();

    return hs.map((h) =>
      JS.is.function(h)
        ? (h as JestHelper.Factory)(globaleEabled)
        : (h as JestHelper)
    );
  })();

  const active = (): boolean => helpers.some((h) => h.active);

  const enter = () => {
    if (!globaleEabled) return;
    helpers.forEach((h) => h.enter());
  };

  const leave = () => {
    if (!globaleEabled) return;
    helpers.forEach((h) => h.leave());
  };

  const catcher: JestHelper['catcher'] = (e) => {
    if (!globaleEabled) return;

    helpers.forEach((h) => h.catcher(e));
  };

  const report = (): string =>
    (globaleEabled &&
      helpers
        .filter((h) => h.active)
        .sort((a, b) => b.priority - a.priority)
        .map((h) => h.report())
        .pack()
        .join('\r\n\r\n')) ||
    undefined;

  const throwReport = () => {
    if (!(globaleEabled && active())) return;

    const reports = report();
    if (reports) throw new FailedContext(reports);
  };

  const attach = () => {
    if (!globaleEabled) return;

    afterEach(throwReport);
  };

  return {
    get active() {
      return active();
    },
    get priority() {
      return helpers.reduce((r, c) => r + c.priority, 0);
    },
    enter,
    leave,
    catcher,
    report,
    throwReport,
    attach,
    it: globaleEabled ? makeIt({ enter, leave, catcher }) : it,
  };
};
