import 'jest';
import { Generics, JS } from '@avstantso/node-or-browser-js--utils';
import { JestHelper } from '../abstract';

export namespace StageHelper {
  export namespace Stage {
    export type Value = string | number;

    export type Overload = {
      (value: StageHelper.Stage.Value): void;
      (
        callback: (prev: StageHelper.Stage.Value) => StageHelper.Stage.Value
      ): void;
    };
  }
}

export interface StageHelper extends JestHelper {
  currentStage: StageHelper.Stage.Value;
  stage: StageHelper.Stage.Overload;
}

export const StageHelper: JestHelper.Factory<StageHelper> = (
  globaleEabled = true
) => {
  let currentStage: StageHelper.Stage.Value = undefined;
  let current: JestHelper.Current;

  const clear = () => {
    currentStage = undefined;
    current = undefined;
  };

  const report = () => {
    return undefined === currentStage || '' === currentStage
      ? undefined
      : `Stage:\r\n${currentStage}`;
  };

  const stage = (param: any) => {
    JS.is.function(param)
      ? (currentStage = Generics.Cast(param)(currentStage))
      : (currentStage = param as StageHelper.Stage.Value);
  };

  const make = (): JestHelper.Current => {
    current = { report };
    return current;
  };

  return Object.defineProperties(
    JestHelper(
      {
        name: StageHelper.name,
        make,
        clear,
        current: () => current,
        get priority() {
          return 100;
        },
      },
      globaleEabled
    ) as StageHelper,
    {
      currentStage: {
        get: () => currentStage,
        set: (value: StageHelper.Stage.Value) => {
          currentStage = value;
        },
      },
      stage: { value: stage, writable: false },
    }
  );
};
