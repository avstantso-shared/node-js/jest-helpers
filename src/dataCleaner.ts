import 'jest';

const forceEach = (callback: () => PromiseLike<any>): void => {
  beforeEach(callback);
  afterEach(callback);
};

const forceAll = (callback: () => PromiseLike<any>): void => {
  beforeAll(callback);
  afterAll(callback);
};

const force = { each: forceEach, all: forceAll };

export const DataCleaner = (noTestDataAfterTest = true) => {
  const each = (callback: () => PromiseLike<any>): void => {
    const f = noTestDataAfterTest ? afterEach : beforeEach;
    f(callback);
  };

  const all = (callback: () => PromiseLike<any>): void => {
    const f = noTestDataAfterTest ? afterAll : beforeAll;
    f(callback);
  };

  return {
    each,
    all,
    force,
  };
};
